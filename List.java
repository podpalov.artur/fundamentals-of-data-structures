public class List {
    int[] array;
    int size;

    public List() {
        size = 0;
    }

    public void add(int item) {
        int[] p2 = array;
        array = new int[size + 1];
        for (int i = 0; i < size; i++) {
            array[i] = p2[i];
        }
        array[size] = item;
        size++;
    }

    public void insert(int item, int index) {
        if (index < 0 || index > size) {
            throw new RuntimeException("Queue is clear.");
        }
        if (index == size) {
            add(item);
            return;
        }
        int[] p2 = array;
        array = new int[size + 1];
        for (int i = 0; i < index; i++) {
            array[i] = p2[i];
        }
        for (int i = index + 1; i < size + 1; i++) {
            array[i] = p2[i - 1];
        }
        array[index] = item;
        size++;
    }

    public int get(int index) {
        if (index < 0 || index > size - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        return array[index];
    }

    public void delete(int index) {
        if (index < 0 || index > size - 1) {
            throw new RuntimeException("Index is invalid.");
        }
        int[] p2 = array;
        array = new int[size - 1];
        for (int i = 0; i < index; i++) {
            array[i] = p2[i];
        }
        for (int i = index; i < size - 1; i++) {
            array[i] = p2[i + 1];
        }
        size--;
    }

    public int getFirst() {
        if (size == 0) {
            throw new RuntimeException("Queue is clear.");
        }
        return array[0];
    }

    public int getSize() {
        return size;
    }

    public void clear() {
        array = null;
        size = 0;
    }

    public void print() {
        for (int i = 0; i < size; i++) {
            System.out.println(get(i));
        }
    }

    public void replace(int item, int index) {
        delete(index);
        insert(item, index);
    }
}

class ListMain {
    public static void main(String[] args) {
        List list = new List();
        list.add(11);
        list.add(2);
        list.add(6344);
        list.add(326);
        list.add(584);
        list.replace(63445, 1);
        list.print();
    }
}